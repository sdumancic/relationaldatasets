import re

class FolFormat:

    def __init__(self):
        pass

    def format(self, definitions, facts):
        return definitions + "\n"*2 + facts

    def format_facts_only(self, definitions, facts):
        return facts



class TweetyFormat:

    def __init__(self):
        pass

    def read_definitions(self, definitions_string):
        vocabulary = {}

        for line in definitions_string.split("\n"):
            if len(line) < 3:
                continue
            else:
                tmp = line.replace(")", "").strip().split("(")
                vocabulary[tmp[0]] = tmp[1].split(",")

        return vocabulary

    def read_db(self, facts_string, predicate_defs):
        facts = {}
        domains = {}

        for line in facts_string.split("\n"):
            if len(line) < 3:
                continue
            else:
                tmp = line.strip().replace(")", "").split("(")
                if tmp[0] not in facts:
                    facts[tmp[0]] = []
                facts[tmp[0]].append(tmp[1])

                for (o, d) in zip(tmp[1].split(","), predicate_defs[tmp[0]]):
                    if d not in domains:
                        domains[d] = set()
                    domains[d].add(o)

        return facts, domains

    @staticmethod
    def formatting_precautions(to_format):
        return to_format.replace("_", "us")

    def print_domains(self, domains):
        final_string = ""

        for dom in domains:
            final_string += self.formatting_precautions("Domain{} = ".format(dom.capitalize()) + "{" + ", ".join(map(lambda x: re.sub(r"\b(\d)+", r"num\1", x.lower()), domains[dom])) + "}\n")

        return final_string

    def print_types(self, defs):
        final_defs = ""

        for pred in defs:
            final_defs += self.formatting_precautions("type({}(Domain{}))\n".format(pred, ",Domain".join(map(lambda x: x.capitalize(), defs[pred]))))

        return final_defs

    def print_tweety_facts(self, facts):
        final_facts = ""

        for pred in facts:
            for gr in facts[pred]:
                final_facts += self.formatting_precautions("{}({})\n".format(pred, ",".join(map(lambda x: re.sub(r"\b(\d)+", r"num\1", x.lower()), gr.split(",")))))

        return final_facts

    def format(self, definitions, facts):
        predicate_definition = self.read_definitions(definitions)
        facts, domains = self.read_db(facts, predicate_definition)

        return "{}\n{}\n{}".format(self.print_domains(domains), self.print_types(predicate_definition), self.print_tweety_facts(facts))

    def format_facts_only(self, definitions, facts):
        predicate_definition = self.read_definitions(definitions)
        facts, domains = self.read_db(facts, predicate_definition)

        return self.print_tweety_facts(facts)
