import os


class Fold:

    def __init__(self, definitions, train_data, test_data, labels_definitions, train_labels, test_labels):
        self.test_filename = ''.join([x.split("/")[-1] for x in test_data])
        self.definitions = open(definitions).read()
        self.labels_definitions = open(labels_definitions).read()
        self.train_folds = '\n'.join([open(x).read() for x in train_data])
        self.test_folds = '\n'.join([open(x).read() for x in test_data])
        self.train_labels = '\n'.join([open(x).read() for x in train_labels])
        self.test_labels = '\n'.join([open(x).read() for x in test_labels])


class GenerativeFold(Fold):

    def __init__(self, definitions, train_data, test_data):
        Fold.__init__(self, definitions, train_data, test_data, definitions, [], [])


class EntireDataset:

    def __init__(self, dataset_filepath, dataset_type, output_folder_name):
        self.dataset_filepath = dataset_filepath
        self.dataset_type = dataset_type
        self.output_folder = output_folder_name

    def make_folds(self, printer):
        folds = os.listdir(self.dataset_filepath + "/" + self.dataset_type + "/data")
        prefix = self.dataset_filepath + "/" + self.dataset_type
        definitions = prefix + "/" + [x for x in os.listdir(self.dataset_filepath + "/" + self.dataset_type) if x.endswith(".def")][0]
        label_defs = prefix + "/labels/" + [x for x in os.listdir(self.dataset_filepath + "/" + self.dataset_type + "/labels") if x.endswith(".def")][0]
        labels = [x.replace("/data/", "/labels/").replace(".db", ".labels") for x in folds]

        contains_all_fold = ""
        if self.dataset_type == 'discriminative':
            contains_all_fold = Fold(definitions, [prefix + "/data/" + x for x in folds], [prefix + "/data/" + x for x in folds], label_defs, [prefix + "/labels/" + x for x in labels], [prefix + "/labels/" + x for x in labels])
        else:
            contains_all_fold = GenerativeFold(definitions, [prefix + "/data/" + x for x in folds], [prefix + "/data/" + x for x in folds])

        os.mkdir(self.output_folder)

        # train file
        file_writer = open(self.output_folder + "/train.db", 'w')
        file_writer.write(printer.format(contains_all_fold.definitions + "\n" + contains_all_fold.labels_definitions, contains_all_fold.train_folds))
        file_writer.close()

        if self.dataset_type == 'discriminative':
            # train labels
            file_writer = open(self.output_folder + "/train.labels", 'w')
            file_writer.write(printer.format_facts_only(contains_all_fold.definitions + "\n" + contains_all_fold.labels_definitions, contains_all_fold.train_labels))
            file_writer.close()


class CrossValidationFixed:

    def __init__(self, dataset_filepath, dataset_type, output_folder_name):
        self.dataset_filepath = dataset_filepath
        self.dataset_type = dataset_type
        self.output_folder = output_folder_name
        self.folds = []

    def prepare(self):
        folds = os.listdir(self.dataset_filepath + "/" + self.dataset_type + "/data")
        prefix = self.dataset_filepath + "/" + self.dataset_type
        definitions = prefix + "/" + [x for x in os.listdir(self.dataset_filepath + "/" + self.dataset_type) if x.endswith(".def")][0]
        label_defs = prefix + "/labels/" + [x for x in os.listdir(self.dataset_filepath + "/" + self.dataset_type + "/labels") if x.endswith(".def")][0]
        labels = [x.replace("/data/", "/labels/").replace(".db", ".labels") for x in folds]

        for test_fold in folds:
            test_labels = test_fold.replace("/data/", "/labels/").replace(".db", ".labels")
            if self.dataset_type == 'discriminative':
                self.folds.append(Fold(definitions, [prefix + "/data/" + x for x in folds if x != test_fold], [prefix + "/data/" + test_fold], label_defs, [prefix + "/labels/" + x for x in labels if x != test_labels], [prefix + "/labels/" + test_labels]))
            else:
                self.folds.append(GenerativeFold(definitions, [prefix + "/data/" + x for x in folds if x != test_fold], [prefix + "/data/" + test_fold]))

    def make_folds(self, printer):
        self.prepare()

        for fold in self.folds:
            os.mkdir(self.output_folder + "/" + fold.test_filename)

            # train file
            file_writer = open(self.output_folder + "/" + fold.test_filename + "/train.db", 'w')
            file_writer.write(printer.format(fold.definitions + "\n" + fold.labels_definitions, fold.train_folds))
            file_writer.close()

            # test file
            file_writer = open(self.output_folder + "/" + fold.test_filename + "/test.db", 'w')
            file_writer.write(printer.format(fold.definitions + "\n" + fold.labels_definitions, fold.test_folds))
            file_writer.close()

            if self.dataset_type == 'discriminative':
                # train labels
                file_writer = open(self.output_folder + "/" + fold.test_filename + "/train.labels", 'w')
                file_writer.write(printer.format_facts_only(fold.definitions + "\n" + fold.labels_definitions, fold.train_labels))
                file_writer.close()

                # test labels
                file_writer = open(self.output_folder + "/" + fold.test_filename + "/test.labels", 'w')
                file_writer.write(printer.format_facts_only(fold.definitions + "\n" + fold.labels_definitions, fold.test_labels))
                file_writer.close()

