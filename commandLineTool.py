import argparse
import os
import settings
from tools.formats import FolFormat, TweetyFormat
from tools.CrossValidation import CrossValidationFixed, EntireDataset


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Command line toolbox for relational dataset manipulation')
    parser.add_argument('--dataset', '-d', help="dataset indicator (name)")
    parser.add_argument('--type', '-t', help="dataset type (generative|discriminative)", default="discriminative", choices=['generative', 'discriminative'])
    parser.add_argument('--list', help='lists all available datasets', action='store_true')
    parser.add_argument('--prepareCVSets', help='prepare the cross validation sets [creates a folder for each fold]', action='store_true')
    parser.add_argument('--output', help='output directory', default='./fold')
    parser.add_argument('--outFormat', help='format for the output files', choices=['tweety', 'fol'], default='fol')
    parser.add_argument('--entireDataset', help='prepares entire dataset without folds', action='store_true')

    args = parser.parse_args()

    output_printer = FolFormat()

    if args.outFormat == 'tweety':
        output_printer = TweetyFormat()

    if args.list:
        print "Available datasets:"
        print '    *' + '\n    *'.join(os.listdir(settings.DATA_ROOT))
    elif args.prepareCVSets:
        print "Preparing cross validation for {} dataset".format(args.dataset)

        cv_creator = CrossValidationFixed(settings.DATA_ROOT + "/" + args.dataset, args.type, args.output)
        cv_creator.make_folds(output_printer)
    elif args.entireDataset:
        print "Fetching entire {} dataset".format(args.dataset)

        dataset = EntireDataset(settings.DATA_ROOT + "/" + args.dataset, args.type, args.output)
        dataset.make_folds(output_printer)

